## About the Pretty Permalink Filter

The Pritty Permalink Filter translates pretty permalinks for WordPress.

[![(License)](https://img.shields.io/badge/license-MIT-blue.svg)][MIT]
[![(Open issues)](https://img.shields.io/bitbucket/issues/vx68k/pretty-permalink-filter.svg)][open issues]
[![(Build status)](https://linuxfront-functions.azurewebsites.net/api/bitbucket/build/vx68k/pretty-permalink-filter?branch=master)][pipelines]

[MIT]: https://opensource.org/licenses/MIT

[Wiki]: https://bitbucket.org/vx68k/pretty-permalink-filter/wiki
[Open issues]: https://bitbucket.org/vx68k/pretty-permalink-filter/issues?status=new&status=open
[Pipelines]: https://bitbucket.org/vx68k/pretty-permalink-filter/addon/pipelines/home
