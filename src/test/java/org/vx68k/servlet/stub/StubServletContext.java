/*
 * StubServletContext - stub class for ServletContext
 * Copyright (C) 2015-2017 Kaz Nishimura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.vx68k.servlet.stub;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.Map;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;

/**
 * Stub class for {@link javax.servlet.ServletContext}.
 *
 * @author Kaz Nishimura
 * @since 3.0
 */
public class StubServletContext implements ServletContext
{
    private static final int MAJOR_VERSION = 3;
    private static final int MINOR_VERSION = 1;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextPath()
    {
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletContext getContext(String path)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMajorVersion()
    {
        return MAJOR_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinorVersion()
    {
        return MINOR_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getEffectiveMajorVersion()
    {
        return MAJOR_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getEffectiveMinorVersion()
    {
        return MINOR_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMimeType(String file)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getResourcePaths(String path)
    {
        return Collections.emptySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public URL getResource(String path) throws MalformedURLException
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getResourceAsStream(String path)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestDispatcher getRequestDispatcher(String path)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestDispatcher getNamedDispatcher(String name)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public Servlet getServlet(String name) throws ServletException
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public Enumeration<Servlet> getServlets()
    {
        return Collections.emptyEnumeration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public Enumeration<String> getServletNames()
    {
        return Collections.emptyEnumeration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(String message)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public void log(Exception exception, String message)
    {
        log(message, exception);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(String message, Throwable throwable)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRealPath(String path)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServerInfo()
    {
        // @todo This method MUST NOT return null.
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInitParameter(String name)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getInitParameterNames()
    {
        return Collections.emptyEnumeration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setInitParameter(String name, String value)
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAttribute(String name)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getAttributeNames()
    {
        return Collections.emptyEnumeration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String name, Object object)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAttribute(String name)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServletContextName()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletRegistration.Dynamic addServlet(String servletName,
                                                  String className)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletRegistration.Dynamic addServlet(String servletName,
                                                  Servlet servlet)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletRegistration.Dynamic addServlet(
        String string, Class<? extends Servlet> servletClass)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Servlet> T createServlet(Class<T> servletClass)
        throws ServletException
    {
        // @todo This method MUST NOT return null.
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletRegistration getServletRegistration(String servletName)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ? extends ServletRegistration> getServletRegistrations()
    {
        return Collections.emptyMap();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterRegistration.Dynamic addFilter(String filterName,
                                                String className)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterRegistration.Dynamic addFilter(String filterName,
                                                Filter filter)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterRegistration.Dynamic addFilter(
        String filterName, Class<? extends Filter> filterClass)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Filter> T createFilter(Class<T> filterClass)
        throws ServletException
    {
        // @todo This method MUST NOT return null.
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterRegistration getFilterRegistration(String filterName)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ? extends FilterRegistration> getFilterRegistrations()
    {
        return Collections.emptyMap();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SessionCookieConfig getSessionCookieConfig()
    {
        // @todo This method MUST NOT return null.
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSessionTrackingModes(
        Set<SessionTrackingMode> sessionTrackingModes)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<SessionTrackingMode> getDefaultSessionTrackingModes()
    {
        return Collections.emptySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes()
    {
        return Collections.emptySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(String className)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends EventListener> void addListener(T listener)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(Class<? extends EventListener> listenerClass)
    {
        try {
            addListener(createListener(listenerClass));
        }
        catch (ServletException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends EventListener> T createListener(Class<T> listenerClass)
        throws ServletException
    {
        try {
            return listenerClass.getDeclaredConstructor().newInstance();
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JspConfigDescriptor getJspConfigDescriptor()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClassLoader getClassLoader()
    {
        return getClass().getClassLoader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void declareRoles(String... roleNames)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVirtualServerName()
    {
        return null;
    }
}
