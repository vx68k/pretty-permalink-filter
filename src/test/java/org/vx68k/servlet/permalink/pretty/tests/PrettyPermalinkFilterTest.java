/*
 * Tests for PrettyPermalinkFilter
 * Copyright (C) 2015-2018 Kaz Nishimura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

package org.vx68k.servlet.permalink.pretty.tests;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vx68k.servlet.permalink.pretty.PrettyPermalinkFilter;
import org.vx68k.servlet.stub.StubServletContext;

/**
 * Tests {@link PrettyPermalinkFilter}.
 *
 * @author Kaz Nishimura
 * @since 3.0
 */
public final class PrettyPermalinkFilterTest {

    private StubServletContext context;
    private StubFilterConfig config;

    /**
     * Sets up a test.
     */
    @Before
    public void setUp() {
        context = new StubServletContext();
        config = new StubFilterConfig(context);
    }

    /**
     * Tears down a test.
     */
    @After
    public void tearDown() {
        config = null;
        context = null;
    }

    /**
     * Tests {@link PrettyPermalinkFilter#init} and
     * {@link PrettyPermalinkFilter#destroy}.
     *
     * @throws javax.servlet.ServletException if any method throws one
     */
    @Test
    public void testInitDestroy() throws ServletException {
        PrettyPermalinkFilter filter = new PrettyPermalinkFilter();
        filter.init(config);
        filter.destroy();
        config.setVerbose(false);
        filter.init(config);
        filter.destroy();
        config.setVerbose(true);
        filter.init(config);
        filter.destroy();
    }

    protected class StubFilterConfig implements FilterConfig {

        private final ServletContext servletContext;
        private final Map<String, String> initParameters;

        public StubFilterConfig(ServletContext servletContext) {
            this.servletContext = servletContext;
            this.initParameters = new HashMap<String, String>();
        }

        void setVerbose(boolean verbose) {
            initParameters.put(PrettyPermalinkFilter.VERBOSE_NAME,
                    Boolean.toString(verbose));
        }

        void resetVerbose() {
            initParameters.remove(PrettyPermalinkFilter.VERBOSE_NAME);
        }

        @Override
        public String getFilterName() {
            return "Pretty Permalink Filter";
        }

        @Override
        public ServletContext getServletContext() {
            return servletContext;
        }

        @Override
        public String getInitParameter(String name) {
            return initParameters.get(name);
        }

        @Override
        public Enumeration<String> getInitParameterNames() {
            return Collections.enumeration(initParameters.keySet());
        }
    }
}
