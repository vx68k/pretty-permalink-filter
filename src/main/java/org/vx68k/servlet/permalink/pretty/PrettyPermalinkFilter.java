/*
 * PrettyPermalinkFilter - pretty permalink filter for WordPress
 * Copyright (C) 2014-2018 Kaz Nishimura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package org.vx68k.servlet.permalink.pretty;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet filter to handle pretty permalinks for WordPress.
 * When WordPress is deployed on a Java EE application server with Quercus,
 * we must implement <em>pretty</em> permalinks differently from normal
 * deployment. This class tries to resolve the issue by intercepting requests
 * before the web container responds with
 * {@link javax.servlet.http.HttpServletResponse#SC_NOT_FOUND}.
 *
 * @author Kaz Nishimura
 * @since 3.0
 */
@WebFilter(filterName = "Pretty Permalink Filter", urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = PrettyPermalinkFilter.VERBOSE_NAME,
            value = "false")
    })
public final class PrettyPermalinkFilter implements Filter {

    /**
     * Name of the initialization parameter to make this object be verbose.
     */
    public static final String VERBOSE_NAME = "VERBOSE";

    /**
     * Path of the default <code>index.php</code> file.
     */
    public static final String INDEX_PHP_PATH = "/index.php";

    /**
     * Reference to the filter configuration passed to {@link #init}.
     */
    private transient FilterConfig storedFilterConfig;

    /**
     * Switch to make this filter be verbose.
     */
    private boolean verbose;

    /**
     * Constructs this object but does nothing.
     */
    public PrettyPermalinkFilter() {
    }

    /**
     * Delegates to {@link javax.servlet.ServletContext#log(String)}.
     *
     * @param message a log message
     */
    protected void log(final String message) {
        storedFilterConfig.getServletContext().log(message);
    }

    /**
     * Delegates to {@link javax.servlet.ServletContext#log(String,
     * Throwable)}.
     *
     * @param message a log message
     * @param throwable a {@link java.lang.Throwable} object
     */
    protected void log(final String message, final Throwable throwable) {
        storedFilterConfig.getServletContext().log(message, throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        storedFilterConfig = filterConfig;
        verbose = Boolean.parseBoolean(
            storedFilterConfig.getInitParameter(VERBOSE_NAME));

        if (verbose) {
            log("PrettyPermalinkFilter initialized");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        storedFilterConfig = null;
    }

    /**
     * {@inheritDoc}
     *
     * Filters servlet requests for pretty permalinks. For HTTP, {@link
     * #doFilter(HttpServletRequest, HttpServletResponse, FilterChain)} does
     * real filtering.
     */
    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response, final FilterChain chain)
            throws ServletException, IOException {
        if (request instanceof HttpServletRequest
                && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;

            doFilter(httpRequest, httpResponse, chain);
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * Filters HTTP servlet requests for pretty permalinks.
     *
     * @param request HTTP servlet request
     * @param response HTTP servlet response
     * @param chain filter chain to pass a request that is not filtered
     * @throws ServletException if a servlet-related exception occurred
     * @throws IOException if an I/O exception occurred
     */
    protected void doFilter(final HttpServletRequest request,
            final HttpServletResponse response, final FilterChain chain)
            throws ServletException, IOException {
        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();

        if (verbose) {
            log("servletPath = \"" + servletPath + "\", pathInfo = \""
                    + pathInfo + "\"");
        }
        if (servletPath.endsWith(INDEX_PHP_PATH)) {
            // Chops the appended last part.  See issue #2.
            int lastSlash = servletPath.lastIndexOf('/');
            if (lastSlash >= 0) {
                servletPath = servletPath.substring(0, lastSlash + 1);
            }
        }
        if (servletPath.startsWith("/wp-") || servletPath.endsWith(".php")) {
            chain.doFilter(request, response);
        } else {
            RequestDispatcher dispatcher
                    = request.getRequestDispatcher(INDEX_PHP_PATH);

            dispatcher.forward(request, response);
        }
    }
}
