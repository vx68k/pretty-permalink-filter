# Description

This directory contatins the source code for a servlet filter that handles
pretty permalinks for [WordPress][] deployed with [Quercus][] on a [Java EE][]
application server.

[WordPress]: <https://wordpress.org/>
[Quercus]: <http://quercus.caucho.com/>
[Java EE]: <https://www.oracle.com/java/technologies/java-ee.html>

This program is *[free software][]*: you can redistribute it and/or modify it
under the terms of the [MIT License] approved by the [Open Source Initiative].

See the [repository wiki][wiki] for more information.

[![(License)](https://img.shields.io/badge/license-MIT-blue.svg)][MIT License]
[![(Open issues)](https://img.shields.io/bitbucket/issues/vx68k/pretty-permalink-filter.svg)][open issues]
[![(Build status)](https://linuxfront-functions.azurewebsites.net/api/bitbucket/build/vx68k/pretty-permalink-filter?branch=master)][pipelines]

[free software]: <http://www.gnu.org/philosophy/free-sw.html>
                 "What is free software?"
[MIT License]: https://opensource.org/licenses/MIT
[Open Source Initiative]: https://opensource.org/

[Wiki]: https://bitbucket.org/vx68k/pretty-permalink-filter/wiki
[Open issues]: https://bitbucket.org/vx68k/pretty-permalink-filter/issues?status=new&status=open
[Pipelines]: https://bitbucket.org/vx68k/pretty-permalink-filter/addon/pipelines/home

## Installation

You should be able to generate a JAR file from the source code with the
contained [Maven][] project if you would like to do it yourself.

[Maven]: <http://maven.apache.org/>

## Known issue

* The servlet filter handles every request as a permalinks unless its servlet
  path starts with `/wp-` or ends with `.php` without testing the existence of
  the resource.
